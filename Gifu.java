import java.util.*;

public class Gifu{
    String name;
    int true_count;
    
    public  Gifu(String name){
        	this.name = name;
    }
    
	public int gifu(int true_count){
	    	//3秒間待機
	    try{
	        for (int i = 3; i > 0; i-- ){
	            Thread.sleep(500);
	           	System.out.println(i);
	       	}
	    	Thread.sleep(500);
	       	System.out.println("START!!");
	       	Thread.sleep(1500);
	   	 }catch(Exception e){
	        	System.out.println("例外発生");
	    }
		
		Map<String, Integer> gi = new HashMap<String, Integer>();
		gi.put("〇", 1);		//1は〇とする
		gi.put("×", 2);	    //２は×とする			
        
        Scanner scan = new Scanner(System.in);  //エンターキーで進む
        
		//問１
		System.out.println("問題1：岐阜県には岐阜弁がある");
		System.out.println("1:〇　2:×");
		int nyuuryoku1 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans1 = gi.get("〇");
		//正誤判断
		if(nyuuryoku1 == ans1){
			System.out.println("正解！おめでとう！");
			true_count++;
		}else{
			System.out.println
					("残念！岐阜弁は老若男女問わず使われているよ！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }

		//問2
		System.out.println("問題2：ユネスコ世界遺産として登録された白川郷を持つ岐阜県、これに認定されたのは２０００年である");
		System.out.println("1:〇　2:×");
		int nyuuryoku2 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans2 = gi.get("×");
		//正誤判断
		if(nyuuryoku2 == ans2){
			System.out.println("正解！１９９５年にユネスコ認定されたよ！");
			true_count++;
		}else{
			System.out.println("残念！答えは１９９５年だよ！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問3
		System.out.println
			("問題3:映画「君の名は」では岐阜県が舞台となっている");
		System.out.println("1:〇　2:×");
		int nyuuryoku3 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans3 = gi.get("〇");
		//正誤判断
		if(nyuuryoku3 == ans3){
			System.out.println("正解！ぜひ一度岐阜に遊びに来てください！");
			true_count++;
		}else{
			System.out.println("残念！岐阜県は自然豊かなのです！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問4
		System.out.println("問題4：岐阜県飛騨・高山高根町で収穫されるメロンと同じ糖度をもつとして称賛されるトウモロコシの名称は？");
		System.out.println("1:飛騨黄金");
		System.out.println("2:タカネコーン");
		System.out.println("3:高山スィート");
		System.out.println("4:岐阜ゴールド");
		int nyuuryoku4 = new Scanner(System.in).nextInt();
		if(nyuuryoku4 == 2){
			System.out.println
				("正解！「高根来ん？」→タカネコーンになったそうです！");
			true_count++;
		}else{
			System.out.println("残念！正解は２です！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }

		
		
		//問5
		System.out.println
			("問題5：飛騨の陶芸品として有名な「さるぼぼ」。ぼぼとは何のことか");
		System.out.println("1:赤ん坊");
		System.out.println("2:蟹");
		System.out.println("3:人形");
		System.out.println("4:お守り");
		int nyuuryoku5 = new Scanner(System.in).nextInt();
		if(nyuuryoku5 == 1){
			System.out.println("正解！さるぼぼ調べてみてね！");
			true_count++;
		}else{
			System.out.println("残念！正解は1です！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
		}
		
		
		//問6
		System.out.println("問題6：岐阜県には〇〇温泉がある。(ヒント：響きは良くないです（笑）)");
		String nyuuryoku6 = new Scanner(System.in).nextLine();
		switch(nyuuryoku6){
		  case "げろ":
		  case "ゲロ":
		  case "下呂":
		System.out.println("正解！温泉自体はとてもきれいだから安心してね！");
			true_count += 2;
			break;
		  default:
			System.out.println("残念！正解は下呂（げろ）です笑");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問7
		System.out.println
				("問題7：岐阜弁で机をつるとは何を意味しているか(ヒント：掃除の時間によく飛び交います");
		String nyuuryoku7 = new Scanner(System.in).nextLine();
		switch(nyuuryoku7){
		  case "運ぶ":
		  case "はこぶ":
		  case "持ち上げる":
			System.out.println("正解！おめでとう！");
			true_count += 2;
			break;
		  default:
			System.out.println("残念！正解は机を運ぶです！");
		}
		
		try{
	        Thread.sleep(1500);
		   System.out.println("終了です！" + this.name + "さんお疲れさまでした！");
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }
	 	return true_count;
		
	}
}