
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.Date;

/**
* localhost上のデータベースと接続し、取得したデータをコンソール出力する。
*/
public class shUseJdbc {
	String name;
	int goukei2;
	
	
    /*接続先サーバー名を"localhost"で与えることを示している*/
    String servername     = "localhost";

    /*接続するデータベース名をsenngokuとしている*/
    String databasename   = "suematsu_db";

    /*データベースの接続に用いるユーザ名をrootユーザとしている*/
    String user = "suematsu";

    /*データベースの接続に用いるユーザのパスワードを指定している*/
    String password = "Prol@b07";

    /*取り扱う文字コードをUTF-8文字としている*/
    String serverencoding = "UTF-8";

    /*データベースをあらわすURLを設定している*/
    String url =  "jdbc:mysql://localhost/" + databasename;

    /*MySQLの場合、URLの形式は次のようになります。
      jdbc:mysql://(サーバ名)/(データベース名)*/

    /*↑データベースをあらわすURL（データベースURL)は、データベースに接続する場合に
      必要となる情報をセットした文字列である。
 		
	/*接続を表すConnectionオブジェクトを初期化*/
    Connection conn = null;
    PreparedStatement ps = null;


	//incet（データベースに記録を書き込み）
	public void incert(String name,int goukei2) {

        try{

            /*クラスローダによりJDBCドライバを読み込んでいることを示している。
            引数は、データベースにアクセスするためのJDBCドライバのクラス名である。*/
            Class.forName( "com.mysql.jdbc.Driver" ).newInstance();

            /*DriverManagerクラスのgetConnectionメソッドを使ってデータベースに接続する。*/
            conn = DriverManager.getConnection( url, user, password );

            System.out.println( "Connected...." );

            /*データベースの接続後に、sql文をデータベースに直接渡すのではなく、
            sqlコンテナの役割を果たすオブジェクトに渡すためのStatementオブジェクトを作成する>。*/
            Statement st = conn.createStatement();

            /*SQL文を作成する*/
            String sqlstr = "insert into Shizuoka(name,goukei2) values (?,?)";
                        //実行するSQL文とパラメータを指定する
                ps = conn.prepareStatement(sqlstr);
                ps.setString(1, name);
                ps.setInt(2,goukei2);


            /*SQL文を実行した結果セットをResultSetオブジェクトに格納している*/
            int result = ps.executeUpdate();

            /*Statementオブジェクトを閉じる*/
        }catch( SQLException e ){

            /*エラーメッセージ出力*/
            System.out.println( "Connection Failed. : " + e.toString() );

            /*例外を投げちゃうぞ*/
            //throw new Exception();

        }catch(IllegalAccessException  e ){

            /*エラーメッセージ出力*/
            System.out.println( "Connection Failed. : " + e.toString() );

        }catch (ClassNotFoundException e){

            /*エラーメッセージ出力*/
            System.out.println("ドライバを読み込めませんでした " + e);
        }catch (InstantiationException e){

            /*エラーメッセージ出力*/
            System.out.println("ドライバを読み込めませんでした " + e);
        }finally{
            try{
                if( conn != null ){
                    conn.close();
                }
            }catch(Exception e){

                /*エラーメッセージ出力*/
                System.out.println( "Exception2! :" + e.toString() );

                /*例外を投げちゃうぞ*/
                //throw new Exception();
            }
        }
    }



    //select（データベースから記録を呼び出し）
    public void select () {

        try{

            /*クラスローダによりJDBCドライバを読み込んでいることを示している。
            引数は、データベースにアクセスするためのJDBCドライバのクラス名である。*/
            Class.forName( "com.mysql.jdbc.Driver" ).newInstance();

            /*DriverManagerクラスのgetConnectionメソッドを使ってデータベースに接続する。*/
            conn = DriverManager.getConnection( url, user, password );

            System.out.println( "【総合得点表】" );

            /*データベースの接続後に、sql文をデータベースに直接渡すのではなく、
            sqlコンテナの役割を果たすオブジェクトに渡すためのStatementオブジェクトを作成する>。*/
            Statement st = conn.createStatement();

            /*SQL文を作成する*/
            String sqlStr = "SELECT * FROM Shizuoka";

            /*SQL文を実行した結果セットをResultSetオブジェクトに格納している*/
            ResultSet result = st.executeQuery( sqlStr );
        	

            /*クエリ結果を1レコードずつ出力していく*/
            while( result.next() )
            {
                /*getString()メソッドは、引数に指定されたフィールド名(列)の値をStringとして取得する*/
                int int1 = result.getInt( "auto_no" );
                String str2 = result.getString( "name" );
                int int3 = result.getInt( "goukei2" );
                                //String str4 = result.getString( "born_era" );
                                //System.out.println( str1 + ", " + str2 + ", " + str3 + "," + str4);
                System.out.println( int1 + "： " + str2 + "さん, " + int3 + "点" );
            }

            /*ResultSetオブジェクトを閉じる*/
            result.close();

            /*Statementオブジェクトを閉じる*/
        }catch(IllegalAccessException  e ){

            /*エラーメッセージ出力*/
            System.out.println( "Connection Failed. : " + e.toString() );

        }catch (InstantiationException e){

            /*エラーメッセージ出力*/
            System.out.println("ドライバを読み込めませんでした " + e);
        	
        }catch( SQLException e ){

            /*エラーメッセージ出力*/
            System.out.println( "Connection Failed. : " + e.toString() );

            /*例外を投げちゃうぞ*/
            // new Exception();

        }catch (ClassNotFoundException e){

            /*エラーメッセージ出力*/
            System.out.println("ドライバを読み込めませんでした " + e);
        }finally{
            try{
                if( conn != null ){
                    conn.close();
                }
            }catch(Exception e){

                /*エラーメッセージ出力*/
                System.out.println( "Exception2! :" + e.toString() );

                /*例外を投げちゃうぞ*/
                //throw new Exception();
            }
        }
    }
}
