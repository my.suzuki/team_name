import java.util.*;

public class Main{
    public static void main(String[] args){
        
		Map<Integer, String> task = new HashMap<Integer, String>();
		task.put(1, "大阪府");
		task.put(2, "静岡県");
		task.put(3, "岐阜県");
		
		
		System.out.println("【都道府県クイズ！】");
		System.out.println("▼Playerの名前を入力してね");
		String name = new Scanner(System.in).nextLine();
		System.out.println("▼問題番号を入力してね");
		System.out.println("1：大阪府");
		System.out.println("2：静岡県");
		System.out.println("3：岐阜県");
		
		int choice = new Scanner(System.in).nextInt();
		
		
		
		switch (choice){
		    
		case 1:
			System.out.print("\033\143");
			System.out.println(task.get(choice) + "の問題を開始します");
	        Osaka o = new Osaka(name);
	        int goukei1 =o.osaka(0);
	        System.out.println(task.get(choice) + "の問題を解いた"+ name + "さんの総合得点：" + goukei1 + "点" );
	        //データの送信
	        UseJdbc uj1 = new UseJdbc();
			uj1.incert(name,goukei1);
			uj1.select();
	        break;
	    case 2:
	    	System.out.print("\033\143");
	    	System.out.println(task.get(choice) + "の問題を開始します");
	        Shizuoka sh = new Shizuoka(name);
	        int goukei2 = sh.shizuoka(0);
	        System.out.println(task.get(choice) + "の問題を解いた"+ name + "さんの総合得点：" + goukei2 + "点" );
			//データの送信
			shUseJdbc uj2 = new shUseJdbc(name, goukei2);
			uj2.incert(); 
			uj2.select();       
	        break;
	    case 3:
	    	System.out.print("\033\143");
	    	System.out.println(task.get(choice) + "の問題を開始します");
	        Gifu gf = new Gifu(name);
	        int goukei3 = gf.gifu(0);
	        System.out.println(task.get(choice) + "の問題を解いた"+ name + "さんの総合得点：" + goukei3 + "点" );
	       	データの送信
	       	shUseJdbc uj3 = new shUseJdbc(name, goukei3);
			uj3.incert();
			uj3.select();
	        break;
	    default:
	        System.out.println("１〜３を選択してね");
	    }


	}
}