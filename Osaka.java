import java.util.*;

public class Osaka{
    String name;
    int true_count;
    
    public Osaka(String name){
        this.name = name;
    }
    
	public int osaka(){
	    	//3秒間待機
	    try{
	        for (int i = 3; i > 0; i-- ){
	            Thread.sleep(500);
	           	System.out.println(i);
	       	}
	    	Thread.sleep(500);
	       	System.out.println("START!!");
	       	Thread.sleep(1500);
	   	 }catch(Exception e){
	        	System.out.println("例外発生");
	    }
		
		Map<String, Integer> o = new HashMap<String, Integer>();
		o.put("〇", 1);			//1は〇とする
		o.put("×", 2);	    	//２は×とする	
		int true_count = 0; 		//正解数
        
        Scanner scan = new Scanner(System.in);  //エンターキーで進む
        
        
        
		//問１
		System.out.println("問題1：大阪は近畿である。");
		System.out.println("1:〇　2:×");
		int nyuuryoku1 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans1 = o.get("〇");
		//正誤判断
		if(nyuuryoku1 == ans1){
			System.out.println("正解！");
			true_count++;
		}else{
			System.out.println("残念！");
		}


	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問2
		System.out.println("問題2：あべのハルカスの展望台は標高250ｍである");
		System.out.println("1:〇　2:×");
		int nyuuryoku2 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans2 = o.get("×");
		//正誤判断
		if(nyuuryoku2 == ans2){
			System.out.println("正解！");
			true_count++;
		}else{
			System.out.println("残念！");
		}


	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問3
		System.out.println("問題3：モータープールは流れるプールのことを指す");
		System.out.println("1:〇　2:×");
		int nyuuryoku3 = new Scanner(System.in).nextInt();
		//〇を正解とするとき
		int ans3 = o.get("×");
		//正誤判断
		if(nyuuryoku3 == ans3){
			System.out.println("正解！");
			true_count++;
		}else{
			System.out.println("残念！");
		}


	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問4
		System.out.println("問題4：大阪で有名な豚まんの店の名前は");
		System.out.println("1:123　2:572×　3：551");
		int nyuuryoku4 = new Scanner(System.in).nextInt();
		//正誤判断
		if(nyuuryoku4 == 3){
			System.out.println("正解！");
			true_count++;
		}else{
			System.out.println("残念！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }

		
		
		//問5
		System.out.println("問題5：通天閣のネオン広告にあるのは？");
		System.out.println("1:日立　2:東芝　3：シャープ");
		int nyuuryoku5 = new Scanner(System.in).nextInt();
		//正誤判断
		if(nyuuryoku5 == 1){
			System.out.println("正解！");
			true_count++;
		}else{
			System.out.println("残念！");
		}


	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }

		
		
		//問6
		System.out.println
			("問題6：道頓堀にあるランナーの名前を入力してください");
		String nyuuryoku6 = new Scanner(System.in).nextLine();
		switch(nyuuryoku6){
		  case "グリコ":
          case "ぐりこ":
		  case "guriko":
			System.out.println("正解！");
			true_count += 2;
			break;
		  default:
			System.out.println("残念！");
		}

	    try{
	        Thread.sleep(1000);
		    System.out.println("Press Enter...");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }



		//問7
		System.out.println
			("問題7：大阪の有名なテーマパークで〇〇〇〇〇〇スタジオジャパンがある");
		String nyuuryoku7 = new Scanner(System.in).nextLine();
		switch(nyuuryoku7){
		  case "ユニバーサル":
          case "universal":
		  case "UNIVERSAL":
		  case "ユニバ":
		  case "ユニバーサルスタジオジャパン":
			System.out.println("正解！");
			true_count += 2;
			break;
		  default:
			System.out.println("残念！");
		}
		
		
		try{
	        Thread.sleep(1500);
		    System.out.println("終了です！お疲れさまでした！");
		    scan.nextLine();
	   	}catch(Exception e){
	        System.out.println("例外発生");
	    }
	
		
		return true_count;

		
		
	}
}
